n_list = [int(i) for i in input().split()]

maxx = n_list.index(max(n_list))
minn = n_list.index(min(n_list))

n_list[maxx],n_list[minn] = n_list[minn],n_list[maxx]

print(n_list)