n = int(input())
n_set = set(range(1,n+1))
while True:
    s = input()
    if s == 'HELP':
        break
    s = {int(x) for x in s.split()}
    ans = input()
    if ans == 'YES':
        n_set &= s
    else:
        n_set &= n_set - s

for i in n_set: print(i,end=' ')