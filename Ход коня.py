x,y = map(int, input().split())
x_1,y_1 = map(int, input().split())

col = abs(x - x_1)
row = abs(y - y_1)

if col == 1 and row == 2 or col == 2 and row == 1 :
    print('YES')
else:
    print('NO')
