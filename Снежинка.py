n = int(input())
a = [["."] * n for i in range(n)]
for i in range(n):
    a[i][i] = "*"
    a[i][n // 2] = "*"
    a[n // 2][i] = "*"
    a[i][(n - 1)- i] = "*"
for row in a:
    print(' '.join([str(elem) for elem in row]))