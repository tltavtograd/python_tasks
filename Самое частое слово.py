n_dict = {}
for i in range(int(input())):
    s = input()
    for item in s.split():
        n_dict[item] = n_dict.get(item, 0)+1

maxx = max(n_dict.values())


for key in sorted(n_dict.keys()):
    if n_dict[key] == maxx:
        print(key)
        break