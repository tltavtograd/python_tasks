n = int(input())
a = [["."] * n for i in range(n)]
for i in range(n):
    for j in range(n):
        a[i][j] = abs(j - i)
for row in a:
    print(' '.join([str(elem) for elem in row]))