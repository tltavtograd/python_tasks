n = int(input())

n_prev,n_next = 0,1
if n == 0:
    print(0)
else:
    i = 1
    while n_next <= n:
        if n_next == n:
            print(i)
            break
        n_prev,n_next = n_next,n_next + n_prev
        i +=1
    else:
        print(-1)