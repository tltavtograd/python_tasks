n = 8
x_list = []
y_list = []

for i in range(n):
    x, y = [int(x) for x in input().split()]
    x_list.append(x)
    y_list.append(y)

correct = True
for i in range(n):
    for j in range(i + 1, n):
        if x_list[i] == x_list[j] or y_list[i] == y_list[j] or abs(x_list[i] - x_list[j]) == abs(y_list[i] - y_list[j]):
            correct = False
if correct:
    print('NO')
else:
    print('YES')
