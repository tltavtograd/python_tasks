n_dict = {}
bull = False
for _ in range(int(input())):
    s = input().split()
    for i in range(len(s)):
        if i != 0:
            n_dict[s[0]] = n_dict.get(s[0],'')+s[i]

for _ in range(int(input())):
    q,f = input().split()
    s = n_dict[f]
    for i in range(len(s)):
        if q == 'write' and s[i] == 'W':
            bull = True
        elif q == 'execute' and s[i] == 'X':
            bull =True
        elif q == 'read' and s[i] == 'R':
            bull = True
    if bull:
        print('OK')
        bull = False
    else:
        print('Access denied')