n_dict = {}
for _ in range(int(input())):
    s = input().replace(',', '').split()
    for i in range(2, len(s)):
        n_dict[s[i]] = n_dict.get(s[i],[]) + [s[0]]

for key in sorted(n_dict.keys()):
    print(key, end=' - ')
    print(*n_dict[key],sep = ', ')
