n,m = map(int,input().split())

n_list = set()
m_list = set()

for i in range(n):
    n_list.add(int(input()))

for i in range(m):
    m_list.add(int(input()))

print(len(n_list.intersection(m_list)))
print(' '.join(str(i) for i in n_list.intersection(m_list)))
print(len(n_list.difference(m_list)))
print(' '.join(str(i) for i in n_list.difference(m_list)))
print(len(m_list.difference(n_list)))
print(' '.join(str(i) for i in m_list.difference(n_list)))
