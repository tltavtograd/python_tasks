x = int(input())
time = 9 * 60

for i in range(1, x + 1):
    if  i % 2 == 0:
        time += 60
    else:
        time += 50

if x % 2 == 0:
    time -= 15
else:
    time -= 5

print(time//60,time%60)