n, k = input().split()
n_list = []
k_list = []

for i in range(1, int(n) + 1):
    n_list.append(i)

for i in range(int(k)):
    k_list = [int(i) for i in input().split()]

    for j in range(k_list[0] - 1, k_list[1]):
        n_list[j] = 0

for i in n_list:

    if i == 0:
        print('.', end='')
    else:
        print('I', end='')
