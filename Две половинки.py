strt = input()

if len(strt) % 2 == 0:
    strt_first = strt[0:int(len(strt) / 2)]
    strt_second = strt[int(len(strt) / 2):len(strt)]
else:
    strt_first = strt[0:int(len(strt) // 2 + 1)]
    strt_second = strt[int(len(strt) // 2 + 1):len(strt)]

print(strt_second + strt_first)