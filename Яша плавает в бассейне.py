n,m,x,y = map(int,input().split())

if n > m:
    print(min(x,abs(n-x),y,abs(m-y)))
else:
    print(min(x,abs(m-x),y,abs(n-y)))