n = int(input())
i = 0

while True:
    i += 1
    if 2 ** i >= n:
        break

print(i - 1, 2 ** (i - 1))