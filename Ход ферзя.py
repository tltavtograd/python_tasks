x,y = map(int,input().split())
x_1,y_1 = map(int,input().split())

in_row = x == x_1
in_col = y == y_1
in_diag = abs(x - x_1) == abs(y - y_1)

if in_row or in_col or in_diag:
    print('YES')
else:
    print('NO')